<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>ログイン画面</title>
</head>

<body>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	
    <h1>ログイン画面</h1>
    
    <form class="form-signin" action="LoginServlet" method="post">
    <p>ログインID<input type="text" name="loginId"></p>
    <p>パスワード<input type="password" name="password"></p>
    <input type="submit" value="ログイン">
    </form>
    
</body>

</html>