<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>

<header>
      <nav class="navbar navbar-inverse">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="userCreate.html">ユーザ管理システム</a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
    </header>

	<a href="UserRegistServlet">新規登録</a>
	
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	
	<form action="UserSearchServlet" method="post">
	<p>
		ログインID<input type="text" name="loginId">
	</p>
	<p>
		ユーザ名<input type="text" name="name">
	</p>
	<p>
		生年月日<input type="date" name="birth">〜<input type="date"
			name="birth2">
	</p>
	<input type="submit" value="検索">
	</form>
	
	<hr>

	<table class="table">
		<thead class="thead-dark">
			<tr>
				<th scope="col">ログインID</th>
				<th scope="col">ユーザ名</th>
				<th scope="col">生年月日</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                    <c:if test="${userInfo.loginId == user.loginId or userInfo.loginId == 'admin'}">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                    </c:if>
                    <c:if test="${userInfo.loginId == 'admin'}" >
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                    </c:if>
                     </td>
                </tr>
			</c:forEach>
		</tbody>
	</table>

</body>

</html>