package controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;


@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public UserUpdateServlet() {
        super();
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User loginSession = (User) session.getAttribute("userInfo");
		
		if(loginSession == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("LoginServlet");
			dispatcher.forward(request, response);
		}
		
		String id = request.getParameter("id");
		
		UserDao userDao = new UserDao();
		User userResult = userDao.idSearch(id);
		
		request.setAttribute("userResult", userResult);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
		dispatcher.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			request.setCharacterEncoding("UTF-8");
			
			String id = request.getParameter("id");
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");
			String name = request.getParameter("name");
			String birth = request.getParameter("birth");
			
			if(!(password.equals(password2)) || name.equals("") || birth.equals("")) {
				
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				
				Date birthDate = java.sql.Date.valueOf(birth);
				
				int Id = Integer.parseInt(id);
				
				User user = new User(Id, loginId, name, birthDate);
				request.setAttribute("userResult", user);
				
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
				dispatcher.forward(request, response);
				
			} else {
			
			UserDao userDao = new UserDao();
			
			userDao.updateUserInfo(password, name, birth, id);
			
			response.sendRedirect("UserListServlet");
			
	    	}
			
	}

}
