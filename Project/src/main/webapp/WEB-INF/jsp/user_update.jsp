<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>

<header>
      <nav class="navbar navbar-inverse">
      	<div class="container">
      		<div class="navbar-header">
            <a class="navbar-brand" href="userCreate.html">ユーザ情報更新システム</a>
      		</div>

          <ul class="nav navbar-nav navbar-right">
            <li class="navbar-text">${userInfo.name} さん </li>
  			<li class="dropdown">
  			  <a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
            </li>
  		  </ul>
      	</div>
      </nav>
    </header>
	
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>

	<form action="UserUpdateServlet" method="post">
		<p>ログインID:${userResult.loginId}</p>
		<input type="hidden" name="loginId" value="${userResult.loginId}">
		<input type="hidden" name="id" value="${userResult.id}">
		<p>
			パスワード<input type="text" name="password">
		</p>
		<p>
			パスワード(確認)<input type="text" name="password2">
		</p>
		<p>
			ユーザ名<input type="text" name="name" value="${userResult.name}">
		</p>
		<p>
			生年月日<input type="date" name="birth" value="${userResult.birthDate}">
		</p>
		<input type="submit" value="更新">
	</form>

	<a href="UserListServlet">戻る</a>
</body>

</html>